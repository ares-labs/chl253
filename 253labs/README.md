Pada challenge labs 253, kita di minta membuat membuat file dengan format, 
nama-angka. Contoh, ayu1, ayu2, ayu3 sampai ayu25 tanpa `hardcode`.

Di sini saya menggunakan `for loop` di `bash` seperti contoh berikut.

```bash
#!/bin/bash
for i in {1..25}; do echo "N "$i > Name$i; done
```

`i`      : Di sebut variable dan bebas penamaannya (umumnya huruf kecil). `i` umum di gunakan di bahasa pemrograman apapun sebagai penggunaan `for loop`. 

`{n..n}` : `..` bisa di artikan `sampai` atau `range`. `{1..25}`berarti 1 sampai 25. Di sini `{1..25}` berperan sebagai value/nilai  dari `i`.

`;`      : `Semicolon` atau titik-koma berfungsi sebagai pemisah command/perintah. 
contoh:
```bash
echo a; echo b
```

Command di atas memberitahukan kepada `bash` bahwa keduanya adalah dua perintah terpisah dan perlu dijalankan secara terpisah satu demi satu. 

`do`  :  Memulai perintah `loop` / starts the loop

`"N"` : String, bisa di isi huruf maupun angka

`>`   : Redirection

`$i`  : `$` memanggil value/nilai variable  `i`  

`done`:  Mengakhiri perintah `loop` / the end of the loop

##### Tanpa redirection

`l` : `alias l='ls -CF'`

![Alt](img/n.png)


##### Redirection

![Alt](img/n-file.png)

### DAPUS:

➟ [Bash Loops](https://ucsbcarpentry.github.io/2021-01-21-SWC-Bash-online/05-loop/)\
➟ [Redirection operator](https://unix.stackexchange.com/a/122980)\
➟ [Separator](https://stackoverflow.com/a/27577487)\
➟ [Bash Reference Sheet](https://mywiki.wooledge.org/BashSheet)\
➟ [Code](https://stackoverflow.com/a/24366441)




--------------------------------------------------------------------
